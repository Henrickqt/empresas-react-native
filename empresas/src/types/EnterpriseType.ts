export default interface EnterpriseType {
  id: number;
  enterprise_type_name: string;
}

export const defaultEnterpriseType: EnterpriseType = {
  id: 0,
  enterprise_type_name: '',
};
