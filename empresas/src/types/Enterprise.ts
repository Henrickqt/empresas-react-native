import EnterpriseType, { defaultEnterpriseType } from './EnterpriseType';

export default interface Enterprise {
  id: number;
  enterprise_name: string;
  photo: string;
  description: string;
  city: string;
  country: string;
  enterprise_type: EnterpriseType;
}

export const defaultEnterprise: Enterprise = {
  id: 0,
  enterprise_name: '',
  photo: '',
  description: '',
  city: '',
  country: '',
  enterprise_type: defaultEnterpriseType,
};
