import React from 'react';
import { Image, Text, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { RectButton } from 'react-native-gesture-handler';

import Enterprise from '../../types/Enterprise';

import styles from './styles';

interface EnterpriseItemProps {
  enterprise: Enterprise;
}

const EnterpriseItem: React.FC<EnterpriseItemProps> = ({ enterprise }) => {
  const { navigate } = useNavigation();

  function handleNavigateToDetail() {
    navigate('EnterpriseDetail', { id: enterprise.id });
  }

  return (
    <RectButton 
      style={styles.container} 
      rippleColor="#E5E5E5" 
      onPress={handleNavigateToDetail}
    >
      <Image 
        style={styles.image} 
        source={{ uri: `https://empresas.ioasys.com.br${enterprise.photo}` }} 
      />

      <View style={styles.info}>
        <Text style={styles.name} numberOfLines={1}>
          {enterprise.enterprise_name}
        </Text>
        <Text style={styles.address} numberOfLines={1}>
          {enterprise.city} - {enterprise.country}
        </Text>
        <Text style={styles.type} numberOfLines={1}>
          {enterprise.enterprise_type.enterprise_type_name}
        </Text>
      </View>
    </RectButton>
  );
}

export default EnterpriseItem;
