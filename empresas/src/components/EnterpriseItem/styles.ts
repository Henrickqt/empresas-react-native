import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderWidth: 1,
    borderColor: '#EDEDED',
    borderRadius: 8,
    marginBottom: 16,
    paddingHorizontal: 12,
    paddingVertical: 12,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },

  image: {
    height: 64,
    width: 64,
    borderRadius: 32,
  },

  info: {
    flex: 1,
    marginLeft: 16,
  },

  name: {
    color: '#5D3F73',
    fontSize: 16,
    fontWeight: '700',
  },

  address: {
    marginTop: 6,
    color: '#A898B4',
    fontSize: 12,
    letterSpacing: 0.6,
  },

  type: {
    marginTop: 4,
    color: '#A898B4',
    fontSize: 12,
    letterSpacing: 0.6,
  },
});

export default styles;
