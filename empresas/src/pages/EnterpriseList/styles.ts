import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F2F8',
  },

  form: {
    width: '100%',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    padding: '5%',
    backgroundColor: '#503662',
  },

  inputGroup: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  inputBlock: {
    width: '48%',
  },

  label: {
    paddingLeft: 4,
    color: '#A898B4',
    letterSpacing: 0.6,
  },

  input: {
    height: 56,
    width: '100%',
    borderRadius: 8,
    marginTop: 4,
    paddingHorizontal: 16,
    backgroundColor: '#755A87',
    color: '#F5F2F8',
    letterSpacing: 0.6,
    justifyContent: 'center',
  },

  button: {
    height: 56,
    width: '100%',
    borderRadius: 8,
    marginTop: 16,
    backgroundColor: '#26A85F',
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonText: {
    fontSize: 16,
    color: '#FFFFFF',
    letterSpacing: 1.2,
    fontWeight: '700',
    textTransform: 'uppercase',
  },

  empty: {
    paddingVertical: 16,
    color: '#5D3F73',
    fontSize: 16,
    letterSpacing: 0.6,
    textAlign: 'center',
  },
});

export default styles;
