import React, { useEffect, useState } from 'react';
import { ScrollView, StatusBar, Text, TextInput, View } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';

import { getUserData } from '../../store/User/selectors';

import EnterpriseItem from '../../components/EnterpriseItem';

import Enterprise from '../../types/Enterprise';
import EnterpriseType from '../../types/EnterpriseType';

import styles from './styles';

function EnterpriseList() {
  const [enterprises, setEnterprises] = useState<Enterprise[]>([]);
  const [enterpriseTypes, setEnterpriseTypes] = useState<EnterpriseType[]>([]);
  const [status, setStatus] = useState('');
  const [name, setName] = useState('');
  const [type, setType] = useState('');

  const userData = useSelector(getUserData);

  useEffect(() => {
    async function indexEnterprise() {
      try {
        const header = new Headers();
        header.append('Content-Type', 'application/json');
        header.append('access-token', userData.access_token);
        header.append('client', userData.client);
        header.append('uid', userData.uid);
        
        const response = await fetch('https://empresas.ioasys.com.br/api/v1/enterprises', {
          method: 'GET',
          headers: header,
        });
  
        const json = await response.json();

        const result: Enterprise[] = json['enterprises'];

        result.forEach((enterprise) => {
          const alreadyExists = enterpriseTypes.some((enterpriseType) => 
            enterpriseType.id === enterprise.enterprise_type.id
          );
    
          if (!alreadyExists) {
            const newArray = enterpriseTypes;
            newArray.push(enterprise.enterprise_type);
            setEnterpriseTypes(newArray);
          }
        });
        
        setStatus(result.length > 0 ? 'Pronto' : 'Nenhuma empresa encontrada');
        setEnterprises(json['enterprises']);
      }
      catch(error) {
        console.log(error);
      }
    }

    setStatus('Carregando...');
    indexEnterprise();
  }, []);

  function getEnterpriseTypeId(typeName: string) {
    if (!typeName) {
      return 0;
    }
    if (Number.isInteger(Number(typeName))) {
      return Number(typeName);
    }

    const result = enterpriseTypes.find((enterpriseType) => 
      enterpriseType.enterprise_type_name.toLowerCase().includes(typeName.toLowerCase())
    );

    return result ? result.id : -1;
  }

  function resolveURL() {
    const baseURL = 'https://empresas.ioasys.com.br/api/v1/enterprises';
    const id = getEnterpriseTypeId(type);
    
    if (id && name) {
      return baseURL + `?enterprise_types=${id}&name=${name}`;
    }
    if (id && !name) {
      return baseURL + `?enterprise_types=${id}`;
    }
    if (!id && name) {
      return baseURL + `?name=${name}`;
    }

    return baseURL;
  }
  
  async function handleSearch() {
    try {
      setStatus('Carregando...');
      setEnterprises([]);

      const header = new Headers();
      header.append('Content-Type', 'application/json');
      header.append('access-token', userData.access_token);
      header.append('client', userData.client);
      header.append('uid', userData.uid);

      const url = resolveURL();
      
      const response = await fetch(url, {
        method: 'GET',
        headers: header,
      });

      const json = await response.json();

      const result: Enterprise[] = json['enterprises'];
      
      setStatus(result.length > 0 ? 'Pronto' : 'Nenhuma empresa encontrada');
      setEnterprises(result);
    }
    catch(error) {
      console.log(error);
    }
  }

  return (
    <View style={styles.container}>
      <StatusBar barStyle='light-content' backgroundColor='#4D2C63' />

      <View style={styles.form}>
        <View style={styles.inputGroup}>
          <View style={styles.inputBlock}>
            <Text style={styles.label}>
              Nome
            </Text>
            <TextInput 
              style={styles.input} 
              placeholder="ex: Enterprise" 
              placeholderTextColor="#B0A2BB" 
              value={name} 
              onChangeText={(text) => {setName(text)}} 
            />
          </View>

          <View style={styles.inputBlock}>
            <Text style={styles.label}>
              Tipo
            </Text>
            <TextInput 
              style={styles.input} 
              placeholder="ex: Fintech" 
              placeholderTextColor="#B0A2BB" 
              value={type} 
              onChangeText={(text) => {setType(text)}} 
            />
          </View>
        </View>

        <RectButton 
          style={styles.button} 
          onPress={handleSearch}
        >
          <Text style={styles.buttonText}>
            Filtrar
          </Text>
        </RectButton>
      </View>
    
      <ScrollView 
        contentContainerStyle={{
          paddingHorizontal: 16,
          paddingVertical: 16,
        }}
      >
        {enterprises.length > 0 
          ? enterprises.map((enterprise: Enterprise) => 
              <EnterpriseItem 
                key={enterprise.id} 
                enterprise={enterprise} 
              />
            )
          : <Text style={styles.empty}>
              {status}
            </Text>
        }
      </ScrollView>
    </View>
  );
}

export default EnterpriseList;
