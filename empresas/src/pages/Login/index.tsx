import React, { useState } from 'react';
import { Alert, Image, StatusBar, Text, TextInput, View } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';

import { storeUserData } from '../../store/User/actions';

import logo from '../../assets/images/logo.png';

import styles from './styles';

function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  
  const { navigate } = useNavigation();
  const dispatch = useDispatch();

  async function handleLogin() {
    if (!email || !password) {
      Alert.alert(
        'Erro',
        'Favor informar seu email e senha antes de prosseguir.',
      );
      return;
    }

    try {
      const header = new Headers();
      header.append('Content-Type', 'application/json');

      const response = await fetch('https://empresas.ioasys.com.br/api/v1/users/auth/sign_in', {
        method: 'POST',
        headers: header,
        body: JSON.stringify({ email, password }),
      });

      if (response.ok) {
        const userData = {
          'access_token': response.headers.get('access-token') as string,
          'client': response.headers.get('client') as string,
          'uid': response.headers.get('uid') as string,
        }

        setEmail('');
        setPassword('');
        dispatch(storeUserData(userData));
        navigate('EnterpriseList');
      }
      else {
        Alert.alert(
          'Erro',
          'Email e/ou senha incorretos.',
        );
      }
    }
    catch (error) {
      console.log(error);
    }
  }

  return (
    <View style={styles.container}>
      <StatusBar barStyle='dark-content' backgroundColor='#F5F2F8' />
      
      <View style={styles.logo}>
        <Image source={logo} style={styles.image} />
      </View>

      <View style={styles.form}>
        <View style={styles.inputBlock}>
          <Text style={styles.label}>
            E-mail
          </Text>
          <TextInput 
            style={styles.input} 
            placeholder="email@exemplo.com" 
            value={email} 
            onChangeText={(text) => {setEmail(text)}} 
          />
        </View>
        
        <View style={styles.inputBlock}>
          <Text style={styles.label}>
            Senha
          </Text>
          <TextInput 
            style={styles.input} 
            placeholder="***************" 
            secureTextEntry={true} 
            value={password} 
            onChangeText={(text) => {setPassword(text)}} 
          />
        </View>

        <RectButton 
          style={styles.button} 
          onPress={handleLogin} 
        >
          <Text style={styles.buttonText}>
            Login
          </Text>
        </RectButton>
      </View>
    </View>
  );
}

export default Login;
