import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
  container: {
    flex: 1,
    padding: '8%',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    backgroundColor: '#F5F2F8',
  },

  logo: {
    width: '100%',
  },

  form: {
    width: '100%',
  },

  image: {
    width: '100%',
    resizeMode: 'contain',
  },

  inputBlock: {
    width: '100%',
  },

  label: {
    paddingLeft: 4,
    color: '#846D94',
    letterSpacing: 0.6,
  },

  input: {
    height: 56,
    width: '100%',
    borderRadius: 8,
    marginTop: 4,
    marginBottom: 16,
    paddingHorizontal: 16,
    letterSpacing: 0.6,
    backgroundColor: '#E8E6EC',
    justifyContent: 'center',
  },

  button: {
    height: 56,
    width: '100%',
    borderRadius: 8,
    marginTop: 16,
    backgroundColor: '#5D3F73',
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonText: {
    fontSize: 16,
    color: '#FFFFFF',
    letterSpacing: 1.2,
    fontWeight: '700',
    textTransform: 'uppercase',
  },
});

export default style;
