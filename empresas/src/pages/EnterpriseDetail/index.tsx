import React, { useEffect, useState } from 'react';
import { Image, ScrollView, StatusBar, Text, View } from 'react-native';
import { useRoute } from '@react-navigation/native';
import { useSelector } from 'react-redux';

import { getUserData } from '../../store/User/selectors';

import Enterprise, { defaultEnterprise } from '../../types/Enterprise';

import styles from './styles';

interface RouteParams {
  id: number;
}

function EnterpriseDetail() {
  const [enterprise, setEnterprise] = useState<Enterprise>(defaultEnterprise);
  const userData = useSelector(getUserData);
  const route = useRoute();

  const params = route.params as RouteParams;

  useEffect(() => {
    async function indexEnterpriseId() {
      try {
        const header = new Headers();
        header.append('Content-Type', 'application/json');
        header.append('access-token', userData.access_token);
        header.append('client', userData.client);
        header.append('uid', userData.uid);
        
        const response = await fetch(`https://empresas.ioasys.com.br/api/v1/enterprises/${params.id}`, {
          method: 'GET',
          headers: header,
        });
  
        const json = await response.json();

        setEnterprise(json['enterprise']);
      }
      catch(error) {
        console.log(error);
      }
    }

    indexEnterpriseId();
  }, []);

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor='#4D2C63' />

      <View style={styles.box}>
        <View style={styles.content}>
          <Image 
            style={styles.image} 
            source={{ uri: `https://empresas.ioasys.com.br${enterprise.photo}` }} 
          />

          <View style={styles.info}>
            <Text style={styles.name} numberOfLines={1}>
              {enterprise.enterprise_name}
            </Text>
            <Text style={styles.address} numberOfLines={1}>
              {enterprise.city} - {enterprise.country}
            </Text>
            <Text style={styles.type} numberOfLines={1}>
              {enterprise.enterprise_type.enterprise_type_name}
            </Text>
          </View>

          <View style={styles.detail}>
            <Text style={styles.label}>
              Descrição
            </Text>
            <ScrollView >
              <Text style={styles.description}>
                {enterprise.description}
              </Text>
            </ScrollView>
          </View>
        </View>
      </View>
    </View>
  );
}

export default EnterpriseDetail;
