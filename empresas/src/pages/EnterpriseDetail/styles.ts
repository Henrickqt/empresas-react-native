import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: '5%',
    backgroundColor: '#F5F2F8',
  },

  box: {
    flex: 1,
    borderWidth: 1,
    borderColor: '#EDEDED',
    borderRadius: 8,
    marginTop: 80,
    paddingHorizontal: '6%',
    backgroundColor: '#FFFFFF',
  },

  content: {
    flex: 1,
    marginTop: -80,
    alignItems: 'center',
  },

  image: {
    width: '100%',
    height: 200,
    borderRadius: 8,
  },

  info: {
    width: '100%',
    paddingVertical: 24,
    alignItems: 'center',
  },

  name: {
    color: '#5D3F73',
    fontSize: 24,
    fontWeight: '700',
    letterSpacing: 0.6,
  },

  address: {
    marginTop: 24,
    color: '#A898B4',
    letterSpacing: 0.6,
  },

  type: {
    marginTop: 16,
    color: '#A898B4',
    letterSpacing: 0.6,
  },

  detail: {
    flex: 1,
    marginTop: 16,
  },

  label: {
    borderBottomWidth: 1,
    borderBottomColor: '#EDEDED',
    paddingLeft: 8,
    paddingBottom: 8,
    color: '#5D3F73',
    fontWeight: '700',
    letterSpacing: 3.2,
    textTransform: 'uppercase',
  },

  description: {
    paddingTop: 8,
    paddingBottom: 16,
    paddingHorizontal: 8,
    color: '#A898B4',
    letterSpacing: 0.6,
    lineHeight: 24,
  },
});

export default styles;
