import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from '../pages/Login';
import EnterpriseList from '../pages/EnterpriseList';
import EnterpriseDetail from '../pages/EnterpriseDetail';

const { Navigator, Screen } = createStackNavigator();

function AppStack() {
  return (
    <NavigationContainer>
      <Navigator 
        screenOptions={{
          headerStyle: {
            backgroundColor: '#5D3F73',
          },
          headerTintColor: '#FFFFFF',
          headerTitleStyle: {
            letterSpacing: 0.8,
          },
        }}
      >
        <Screen 
          name="Login" 
          component={Login} 
          options={{ 
            headerShown: false, 
          }} 
        />
        <Screen 
          name="EnterpriseList" 
          component={EnterpriseList} 
          options={{
            title: 'Lista de empresas',
          }} 
        />
        <Screen 
          name="EnterpriseDetail" 
          component={EnterpriseDetail} 
          options={{
            title: 'Detalhe da empresa',
          }} 
        />
      </Navigator>
    </NavigationContainer>
  );
}

export default AppStack;
