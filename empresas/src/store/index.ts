import { combineReducers, createStore } from 'redux';
import userReducer from './User/reducers';

const rootReducer = combineReducers({
  user: userReducer,
});

const store = createStore(rootReducer);

export default store;
