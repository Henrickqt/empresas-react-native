import { UserReducer } from './types';

export function getUserData(state: UserReducer) {
  return state.user;
}
