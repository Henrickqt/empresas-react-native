import { UserDataProps, UserDataTypes } from './types';

export function storeUserData(userData: UserDataProps) {
  return {
    type: UserDataTypes.STORE_USER_DATA,
    payload: userData,
  };
};

export function deleteUserData() {
  return {
    type: UserDataTypes.DELETE_USER_DATA,
    payload: null,
  }
};
