export enum UserDataTypes {
  STORE_USER_DATA = 'STORE_USER_DATA',
  DELETE_USER_DATA = 'DELETE_USER_DATA',
};

export interface UserDataProps {
  'access_token': string;
  'client': string;
  'uid': string;
};

export interface Action {
  type: UserDataTypes;
  payload: UserDataProps | null;
};

export interface UserReducer {
  user: UserDataProps
};
