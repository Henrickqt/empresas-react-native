import { Action, UserDataProps, UserDataTypes } from './types';

const INITIAL_STATE: UserDataProps = {
  access_token: '',
  client: '',
  uid: '',
};

export default function userReducer(state = INITIAL_STATE, action: Action) {
  switch (action.type) {
    case UserDataTypes.STORE_USER_DATA: 
      return action.payload;
    case UserDataTypes.DELETE_USER_DATA: 
      return INITIAL_STATE;
    default: 
      return state;
  }
};
