import React from 'react';
import { Provider } from 'react-redux';

import AppStack from './src/routes/AppStack';
import store from './src/store'

function App() {
  return (
    <Provider store={store}>
      <AppStack />
    </Provider>
  );
}

export default App;
