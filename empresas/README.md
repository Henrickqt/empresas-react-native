# Empresas - Desafio React Native

Projeto desenvolvido de acordo com o **Desafio React Native** da empresa [ioasys](https://ioasys.gupy.io/).

![](https://bitbucket.org/Henrickqt/empresas-react-native/raw/bfa965121d5de4ac5517d16c950d167a6f5cfb9a/assets/cover.jpg)

## Mobile (Frontend)

Este aplicativo móvel foi criado usando as seguintes bibliotecas/tecnologias:

- [TypeScript](https://www.typescriptlang.org/): é um superset de JavaScript que adiciona tipagem e alguns outros recursos a linguagem, facilitando o desenvolvimento de projetos;

- [React-Native](https://reactnative.dev/): é usada para desenvolver aplicativos para os sistemas Android e iOS de forma nativa;

- [Expo](https://docs.expo.io/): é uma plataforma para desenvolvimento de aplicativos nativos universais para Android e iOS, utilizando JavaScript/TypeScript e React Native;

- [React Navigation](https://reactnavigation.org/): é usado para realizar a navegação entre as telas do aplicativo React Native;

- [Redux](https://reactnavigation.org/): permite centralizar o estado e a lógica do aplicativo, facilitando o seu uso e evitando o _Prop Drilling_;

- [React Redux](https://react-redux.js.org/): é responsável por fazer a ligação do React com o Redux, permitindo que os componentes React leiam dados de um _store_ e também despache ações para o esse _store_, a fim de atualizar os dados.

### Execução
Caso não tenha o [Yarn](https://classic.yarnpkg.com/pt-BR/) instalado, sugiro a sua utilização.

- Após baixar o projeto, execute `yarn install` para instalar todas as dependências utilizadas;

- Execute `yarn start` para iniciar o Metro Bundler no navegador;

- Para executar o aplicativo, você pode baixar o aplicativo Expo da PlayStore/AppStore e instalá-lo em seu smartphone;

- Para finalizar, faça a leitura do QR-Code utilizando o aplicativo Expo.

Lembrando que seu smartphone precisa estar na mesma rede do computador que está executando o Metro Bundler.
